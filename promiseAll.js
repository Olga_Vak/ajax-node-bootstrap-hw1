async function requestFilms (){
    const result = await fetch('https://swapi.co/api/films/');
    const data = await result.json();
    console.log(data);
    return data;
}

async function requestCharacters(url){
    const result = await fetch (url);
    const data = await result.json();
    console.log(data);
    return data;
}

requestFilms()
    .then(resolve => renderList(resolve.results))
// .then(resolve => {
//     const charactersArray = [];
//     console.log(resolve);
//     resolve.forEach((el)=>{
//         charactersArray.push(el.characters);
//     })
//     console.log(charactersArray);
//     return charactersArray;
// }).then(array => Promise.all(requestCharacters(array)).then(results => console.log(results));



const container = document.querySelector('.container');

function renderList(list) {
    const content = document.createElement('div');
    content.className = 'content';
    list.forEach(el =>{
        const listItem = document.createElement('div');
        const itemName = document.createElement('h3');
        const itemDescription = document.createElement('div');
        const inner = `<p>Эпизод ${el.episode_id}</p><h4>Краткое содержание фильма:</h4><p>${el.opening_crawl}</p>`;
        itemName.innerText = el.title;
        itemDescription.innerHTML = inner;
        const charactersArray = [];
        const requestArray = [];
        const charactersList = document.createElement('div');
        // let promise = Promise.all(el.characters)
        //     .then(resolve => console.log(resolve));
        el.characters.forEach(item => {
            let request = fetch(`${item}`);
            requestArray.push(request);
        });
        Promise.all(requestArray)
            .then(resolve => {
                resolve.forEach(res =>{
                    console.log(res.json());

                    renderCharacterList(res.json(),charactersArray,charactersList)
                })
            });

        content.appendChild(listItem);
        listItem.appendChild(itemName);
        listItem.appendChild(itemDescription);
        listItem.appendChild(charactersList);
    });
    container.appendChild(content);
    return list;
}

function renderCharacterList (item,array,list){
    // const charactersArray = [];
    // const charactersList = document.createElement('div');
    array.push(`${item.name}`);
    list.innerHTML = `<h4>Список персонажей:</h4>${array.join(', ')}`
}

// function renderCharacterList (item, array, list){
//     const requestCharacters = new XMLHttpRequest();
//     requestCharacters.open('GET',`${item}`, true);
//     requestCharacters.onreadystatechange = () =>{
//         if (requestCharacters.readyState === 4){
//             let response = JSON.parse(requestCharacters.response);
//             array.push(`${response.name}`);
//             list.innerHTML = `<h4>Список персонажей:</h4>${array.join(', ')}`
//         }
//     };
//     requestCharacters.send()
// }