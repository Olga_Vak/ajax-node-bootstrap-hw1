const container = document.querySelector('.container');

async function requestFilms (){
    const result = await fetch('https://swapi.co/api/films/');
    const data = await result.json();
    console.log(data);
    return data;
}

async function requestCharacters(url){
    const result = await fetch (url);
    const data = await result.json();
    return data;
}

requestFilms().then(resolve => renderList(resolve.results));

function renderList(list) {
    const content = document.createElement('div');
    content.className = 'content';
    list.forEach(el =>{
        const listItem = document.createElement('div');
        const itemName = document.createElement('h3');
        const itemDescription = document.createElement('div');
        const inner = `<p>Эпизод ${el.episode_id}</p><h4>Краткое содержание фильма:</h4><p>${el.opening_crawl}</p>`;
        itemName.innerText = el.title;
        itemDescription.innerHTML = inner;
        const charactersArray = [];
        const charactersList = document.createElement('div');
        el.characters.forEach(item => {
               requestCharacters(`${item}`)
                   .then(data => renderCharacterList(data, charactersArray, charactersList))
            });

        content.appendChild(listItem);
        listItem.appendChild(itemName);
        listItem.appendChild(itemDescription);
        listItem.appendChild(charactersList);
    });
    container.appendChild(content);
    return list;
}

function renderCharacterList (item,array,list){
        array.push(`${item.name}`);
        list.innerHTML = `<h4>Список персонажей:</h4>${array.join(', ')}`
}
