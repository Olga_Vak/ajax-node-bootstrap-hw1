let request = new XMLHttpRequest();

request.onreadystatechange = ()=>{
    if(request.readyState === 4){
        let response = request.response;
        let objJSON = JSON.parse(response);
        renderList(objJSON.results);
    }
};

request.open('GET', 'https://swapi.co/api/films/', true);
request.send();



const container = document.querySelector('.container');

function renderList(list) {
    const content = document.createElement('div');
    content.className = 'content';

    list.forEach(el =>{
        const listItem = document.createElement('div');
        const itemName = document.createElement('h3');
        const itemDescription = document.createElement('div');
        const inner = `<p>Эпизод ${el.episode_id}</p><h4>Краткое содержание фильма:</h4><p>${el.opening_crawl}</p>`;
        itemName.innerText = el.title;
        itemDescription.innerHTML = inner;
        const charactersArray = [];
        const charactersList = document.createElement('div');
        el.characters.forEach(item => {
            renderCharacterList(item, charactersArray, charactersList);
        });
        content.appendChild(listItem);
        listItem.appendChild(itemName);
        listItem.appendChild(itemDescription);
        listItem.appendChild(charactersList);
    });
    container.appendChild(content);
}


function renderCharacterList (item, array, list){
    const requestCharacters = new XMLHttpRequest();
    requestCharacters.open('GET',`${item}`, true);
    requestCharacters.onreadystatechange = () =>{
        if (requestCharacters.readyState === 4){
            let response = JSON.parse(requestCharacters.response);
            array.push(`${response.name}`);
            list.innerHTML = `<h4>Список персонажей:</h4>${array.join(', ')}`
        }
    };
    requestCharacters.send()
}

